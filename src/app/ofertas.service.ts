import {Oferta} from './shared/oferta.model'
import {Http, Response} from '@angular/http'
import { Injectable } from '@angular/core'

//habilitar conversao de observable para promise
import 'rxjs/add/operator/toPromise'

import {URL_API} from './app.api'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/retry'

@Injectable()
export class OfertasService {
    
    constructor(private http: Http){

    }
    
    public getOfertas(): Promise<Oferta[]> {
        
        return this.http.get(`${URL_API}/ofertas?destaque=true`)
            .toPromise()
            .then((resposta: Response)=> resposta.json())
    }

    public getOfertasPorCategoria(categoria:string): Promise<Oferta[]> {
        
        return this.http.get(`${URL_API}/ofertas?categoria=${categoria}`)
            .toPromise()
            .then((resposta: Response)=> resposta.json())
    }

    //Preciso converter de array de objeto para um objeto
    public getOfertaPorId(id:number){
        return this.http.get(`${URL_API}/ofertas?id=${id}`)
            .toPromise()
            .then((resposta: Response) => {                
                return resposta.json()[0]
            })
    }

    public getComoUsarOfertaPorId(id: number): Promise<string>{
        return this.http.get(`${URL_API}/como-usar?id=${id}`)
            .toPromise()
            .then((resposta: Response)=>{
                return resposta.json()[0].descricao
            })
    }

    public getOndeFicaOfertaPorId(id: number): Promise<string>{
        return this.http.get(`${URL_API}/onde-fica?id=${id}`)
            .toPromise()
            .then((resposta: Response)=>{
                return resposta.json()[0].descricao
            })
    }
    
    public pesquisaOfertas(termo: string): Observable<Oferta[]>{
        return this.http.get(`${URL_API}/ofertas?descricao_oferta_like=${termo}`)
            .retry(10)
            .map((resposta: Response) => { return resposta.json()})
            //convertendo Observable Response para um Observable Json com map
    }
}