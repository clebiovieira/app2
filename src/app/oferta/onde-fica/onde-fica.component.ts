import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { OfertasService } from '../../ofertas.service'
import { Params } from '@angular/router/src/shared';

@Component({
  selector: 'app-onde-fica',
  templateUrl: './onde-fica.component.html',
  styleUrls: ['./onde-fica.component.css'],
  providers: [OfertasService]
})
export class OndeFicaComponent implements OnInit {

  public ondeFica = ''

  constructor(
    private route: ActivatedRoute,
    private ofertasService: OfertasService) { }

  ngOnInit() {
    //Escutar se rotas do pai estão sendo modificadas
    this.route.parent.params.subscribe((parametros:Params)=>{
      this.ofertasService.getOndeFicaOfertaPorId(parametros.id)
      .then((descricao: string) => {
        this.ondeFica = descricao
      })
    })
  }

}
