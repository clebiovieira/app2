import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import {OfertasService} from '../../ofertas.service'
import { Params } from '@angular/router/src/shared';


@Component({
  selector: 'app-como-usar',
  templateUrl: './como-usar.component.html',
  styleUrls: ['./como-usar.component.css'],
  providers:[OfertasService]
})
export class ComoUsarComponent implements OnInit {

  public comoUsar: string = ''

  constructor(
    private route: ActivatedRoute,
    private ofertasService: OfertasService) { }

  ngOnInit() {
    //Escutar se rotas do pai estão sendo modificadas
    this.route.parent.params.subscribe((parametros:Params)=>{
      this.ofertasService.getComoUsarOfertaPorId(parametros.id)
      .then((descricao: string) => {
        this.comoUsar = descricao
      })
    })
  }

}
